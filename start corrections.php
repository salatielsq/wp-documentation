<!-- Documentação do Wordpress simplificada por  Salatiel. Enjoy! -->

<!-- Correção do topo css -->
/*
Theme Name: My Blog WP Theme
Theme URI: localhost
Description: The simple theme wordpress
Author: Salatiel Queiroz
Author URI: www.fb.com/salatiel.queiroz
Version: 1.0
Tags: branco, vermelho, preto
*/

<!-- Pega o Header -->
<?php get_header(); ?>

<!-- Pega o Footer -->
<?php get_footer(); ?>

<!-- Pega o Sidebar -->
<?php get_sidebar(); ?>

<!-- Pega o arquvio externo -->
<?php include (TEMPLATEPATH '/arquivo.php'); ?>

<!-- Corregação do link para o style.css -->
<style type="text/css">
	@import url( <?php bloginfo('stylesheet_url'); ?> );
</style>

<!-- Correção para incluir scripts dos plugins -->
<?php

	wp_head();
?>
</head><!-- Coloque logo acima do head -->

<?php

	wp_footer();
?>
</body><!-- Coloque logo acima do body -->

<!-- 
Corige o link para o template
	Colocar sempre que tiver que pegar um arquivo que vem da pasta do template.
	Ex.:
	<img src="<?php bloginfo('template_directory'); ?>/pasta/imagem.jpg" />
-->
<?php bloginfo('template_directory'); ?>

<!-- Pega o link para o HOME -->
<?php echo get_settings('home'); ?>


<!-- Recupera do blog, geralmente cadastrados no painel admin do wordpress -->
<?php bloginfo(); ?>

<!-- Recupera o titulo, seja da página, ou post, arquivo e etc -->
<?php wp_title(); ?> / <?php wp_title('|'); ?>
<!-- Titulo personalizado -->
<?php if(is_home()){ bloginfo('name') }
	  elseif (is_category()) { single_cat_title(); echo ' - '; bloginfo('name'); }
	  elseif (is_single();) { single_post_title(); }
	  elseif (is_page();) { bloginfo('name'); echo ' | '; single_post_title(); }
	  else { wp_title('', true) }
?>

