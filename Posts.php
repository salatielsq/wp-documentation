<!-- Documentação do Wordpress simplificada por  Salatiel. Enjoy! -->

<!--
	Query post:
	 - Excluir cat $car= -IdDaCat
	 - Apenas de uma cat $cat= IdDaCat
	 - Inicio do post Limit showposts = 1 (pode repetir a noticia)
	 - Limite offset= 1 (De onde começa a contar, 0 = Primeiro post, 1 = Segundo post)
	 - Pegar categoria por nome = category_name=Nome da Categoria 
-->
<?php query_post('showposts=1&category_name=nomeDaCategoria'); ?>

<!-- Abre o loop de posts -->
<?php if (have_posts()): while (have_posts()): the_post(); ?>

<!-- Fecha o loop -->
<?php endwhile; else: ?>
<?php endif ?>

<!-- Pega o link do post -->
<?php the_permalink(); ?>

<!-- Pega o titulo do post -->
<?php the_title(); ?>

<!-- Pega a data (J=Dia, M=Mês, Y=Ano) -->
<?php the_time('J M Y'); ?>

<!-- Pega o autor da postagem -->
<?php the_author(); ?>

<!-- Pega conteúdo do post -->
<?php the_content(); ?>

<!-- Pega o thumbnail ou imagem do post -->
<?php the_post_thumbnail(); ?>

