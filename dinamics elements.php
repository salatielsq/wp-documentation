<!-- Documentação do Wordpress simplificada por  Salatiel. Enjoy! -->

<!-- Habilitando o menu no functions.php -->
<?php

if ( function_exists( ‘register_nav_menu’ ) ) {
	register_nav_menu( ‘meu_menu’, ‘Este é meu primeiro menu’ );
	register_nav_menu( ‘segundo_menu’, ‘Este é meu segundo menu’ );
}

?>





<?php 
wp_nav_menu( array(
	'menu' => 'meu_menu',//O nome do menu em que deseja retornar (valor determinado na função register_menu)
	'theme_location' => 'meu_menu',//Onde vai está localizado o menu (valor determinado na função register_menu)
	'container' => 'div',//Se alguma tag irá envolver a lista do menu
	'container_class' => 'classe_do_container',//Classes do container
	'container_id' => 'id_do_container',//O ID do container
	'menu_class' => 'classe_do_menu',//Classes da lista do menu
	'echo' => true,//Usado para retorno do menu, em uma vez que seu valor seja falso, o menu não será retornado ou visível.
	'menu_id' => 'id_do_menu',//O ID da lista do menu
	'before' => '',//Se algum elemento entrará antes do menu
	'after' => '',//Se algum elemento entrará depois do menu
	'link_before' => '',//Se algum elemento entrará antes do link
	'link_after' => '',//Se alguém elementro entrará depois do menu
	'depth' => 0,//Quantos níveis de hierarquia devem ser incluídos
	'walker' => '',//Objeto para personalização do menu
));

//Menu Simples:
wp_nav_menu(array(
	'menu' => 'menu_principal',
	'theme_location' => 'menu_principal',
	'echo' => true,
	'container' => 'nav',
	'container_id' => 'menu' 
));

?>